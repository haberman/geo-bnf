const xhr_call = (url, options) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest()
        const defaults = {
            method: 'GET',
            responseType: ''
        }

        let opt = options || {}

        if (url === undefined) { reject("url is missing") }

        if (opt.method === undefined) { opt.method = defaults.method }
        xhr.method = opt.method

        if (opt.responseType === undefined) { opt.responseType = defaults.responseType }
        xhr.responseType = opt.responseType

        xhr.onload = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                if (opt.responseType === '') { resolve(JSON.parse(xhr.responseText)) }
                else if (opt.responseType === 'text') { resolve((new DOMParser()).parseFromString(xhr.responseText, 'text/xml')) }
                else { resolve(xhr.response) }
            }
        }

        if (opt.method === 'GET') {
            if (opt.data !== undefined) { url += encode_to_url(opt.data) }

            xhr.open('GET', url)
            xhr.send()
        } else if (opt.method === 'POST') {
            xhr.open('POST', url)
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded')

            if (opt.data !== undefined) { xhr.send(encode_to_url(opt.data, 'POST')) }
            else { xhr.send() }
        }
    })
}

const encode_to_url = (data, method = 'GET') => {
    let i, k, v, url

    url = method === 'GET' ? '?' : ''
    i = 0

    for (k in data) {
        v = data[k]

        if (i !== 0) { url += '&' }
        if (!is_object(v)) { url += k + '=' + encodeURIComponent(v) }

        i++
    }

    return url
}

const is_object = obj => {
    const type = typeof obj
    return type === 'function' || type === 'object' && !!obj
}
