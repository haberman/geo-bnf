# GeoBNF

### installation
- creates a `private.js` with the required content `MAPBOX_TOKEN = 'your_mapbox_access_token'`
- serves with node [`serve`](https://www.npmjs.com/package/serve) command or host on some server


### TODO's
- [ ] add a type filter
- [ ] enable to switch relation qualifier to `any` & `all` in order to widen queries
- [ ] makes a better UX: notifies when no results, loading states...
