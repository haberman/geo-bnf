SPARQL_GATE = 'http://ext.penumbra.me/scripts/sparql_galligate.php'
SRUAPI_GATE = 'http://ext.penumbra.me/scripts/sruapi_galligate.php'

const has_active_place = () => {
    const docs = document.querySelector('#docs')

    if (docs.classList.contains('open')) {
        return docs.getAttribute('data-ark')
    } else { return null }
}

const create_map = (style, container, center, zoom) =>
    new mapboxgl.Map({
        container: container,
        style: style,
        center: center,
        zoom: zoom
    })

/** Converts an sru xml with places to geojson feature collection. */
const places_to_geojson = res => {
    const geojson = { type: 'FeatureCollection', features: [] }

    for (let k of res.results.bindings) {
        geojson.features.push({
            type: 'Feature',
            properties: {
                label: k.place_label.value,
                uri: k.place.value
            },
            geometry: {
                type: 'Point',
                coordinates: [k.lng.value, k.lat.value]
            }
        })
    }

    return geojson
}

/** Returns the subquery filtering to enclose a query within a given geographical bounding box. */
const filter_bbox = bbox => `FILTER(?lng<${bbox.getEast()})
                             FILTER(?lng>${bbox.getWest()})
                             FILTER(?lat>${bbox.getSouth()})
                             FILTER(?lat<${bbox.getNorth()})`

/** Returns the subquery limiting the amount of results starting at a given offset. */
const offset_limit = (offset, limit) => `OFFSET ${offset} LIMIT ${limit}`

/** Query the SPARL gate to get places the BNF knows about inside a given bbox. */
const query_sparql_places = async (bbox, offset, limit) => {
    const q = `SELECT DISTINCT ?place ?place_label ?lat ?lng
    WHERE {
        ?geoconcept skos:closeMatch ?rameau;
            skos:prefLabel ?place_label;
            foaf:focus ?place.
        ?rameau a skos:Concept.
        ?place rdf:type geo:SpatialThing;
            geo:lat ?lat;
            geo:long ?lng.
            ${filter_bbox(bbox)}
    } ${offset_limit(offset, limit)}`.replace(/\s{2,}/g, ' ') // compress query

    const res = await xhr_call(`${SPARQL_GATE}`, { data: { query: q } })
    return places_to_geojson(res)
}

/**
 * Maybe this would be a way to get rid of SRU
 * PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX marcrel: <http://id.loc.gov/vocabulary/relators/>
PREFIX bnfroles: <http://data.bnf.fr/vocabulary/roles/>
SELECT ?lieu ?lat ?lon ?label ?gallicaUrl ?titre
WHERE
{
  ?conceptLieu foaf:focus ?lieu;
    skos:prefLabel ?label .
  ?lieu a geo:SpatialThing;
    geo:lat ?lat ;
    geo:long ?lon.
  ?conceptLieu foaf:focus <http://data.bnf.fr/ark:/12148/cb15203523z#about>.
    
  ?conceptLieu  skos:closeMatch ?sujet.
  ?edition dcterms:title ?titre.
  ?edition dcterms:subject ?sujet ;
    rdarelationships:expressionManifested ?exp.
  ?exp ?s ?p .
#  ?edition rdarelationships:electronicReproduction <https://gallica.bnf.fr/ark:/12148/btv1b53062329r>.
  ?edition rdarelationships:electronicReproduction ?gallicaUrl.
}
LIMIT 1

 */
/** @unimplemented @todo query docs from SPARQL with a place UID/URL and not by filtering on label */
const query_sparql_docs = async (label, offset, limit) => {
    const q = `SELECT ?docnum ?title ?date ?place_label
    WHERE {
      ?geoconcept foaf:focus ?place;
        skos:prefLabel ?place_label.
        FILTER contains(?place_label, '${label}')
      ?place rdf:type geo:SpatialThing.
      ?geoconcept skos:closeMatch ?subject.
      ?edition dcterms:subject ?subject;
        rdarelationships:expressionManifested ?exp.
      OPTIONAL { ?edition rdarelationships:electronicReproduction ?docnum. }
      OPTIONAL { ?edition dcterms:date ?date. }
      OPTIONAL { ?edition dcterms:title ?title. }
    } ${offset_limit(offset, limit)}`

    const res = await xhr_call(SPARQL_GATE, { data: { query: q } })
    return res
}

// const query_iiif = async () => {}

/**
 * Query the SRUAPI gate on a given `bib.subject`.
 * 
 * @param subject the `bib.subject` value
 * @param props a 2d list forming additional conditions (`[[{and|or}, search_key, relation, value], ...]`)
 * @returns An xml (DOMDocument) representing data results.
 * 
 * @todo add another parameter to change the relation type of main subject
 */
const query_sru = async (subject, props) => {
    let q = `bib.subject adj "${subject}"`
    // q += ` and (bib.doctype any)`

    if (props) {
        for (let prop of props) { q += ` ${prop[0]} (${prop[1]} ${prop[2]} "${prop[3]}")` }
    }

    const res = await xhr_call(SRUAPI_GATE, {
        responseType: 'text',
        data: { query: q, max_records: 50 }
    })

    return res
}

/** Refreshes the place layer when the user is not focused on an active place. */
const refresh_data = async (map, src) => {
    if (has_active_place() !== null) { return null }

    const places = await query_sparql_places(map.getBounds(), 0, 100)
    map.getSource(src).setData(places)

    return places
}

/** Refreshes the DOM of documents retrieved about the current active place. */
const update_docs = (properties, docs) => {
    document.querySelector('#docs').setAttribute('data-ark', properties.uri)
    document.querySelector('#docs h2').innerHTML = `${docs.length} results`

    let li = ''
    for (let doc of docs) {
        // console.log(doc)
        const id = doc.getElementsByTagName('srw:recordIdentifier')[0].innerHTML
        // const dc_id = doc.getElementsByTagName('dc:identifier')[0].innerHTML
        try {
            // const img = new Image()
            // img.onload = e => { console.log(e) }
            // img.src = `http://gallica.bnf.fr/${id}/lowres`
            // http://gallica.bnf.fr/ark:/12148/btv1b530606400.thumbnail.highres
            //     url = "myimg.jpg",
            //     container = document.getElementById("holder-div");

            // img.onload = function () { container.appendChild(img); };
            // img.src = url;
            li += `<li data-identifier="${id}">`

            li += `<h3>${doc.getElementsByTagName('dc:date')[0].innerHTML}</h3>`
            li += `<p>${doc.getElementsByTagName('dc:title')[0].innerHTML}</p>`
            li += `<span><a href="https://gallica.bnf.fr/${id}">link</a></span>`

            li += `</li>`
        } catch (e) { continue; }
    }

    document.querySelector('#docs ul').innerHTML = li
    document.querySelector('#docs').classList.add('open')
}

/** Inits the application once the DOM is loaded. */
document.addEventListener('DOMContentLoaded', async () => {
    mapboxgl.accessToken = MAPBOX_TOKEN // should be found inside private.js

    // const map = create_map('mapbox://styles/mapbox/outdoors-v9', 'map', [2.3514992, 48.8566101], 12)
    const map = create_map('mapbox://styles/penumbra/cjmvr3910bvpk2rlcpljx0dd6', 'map', [2.3514992, 48.8566101], 12)

    /** Hover popup when browing places. */
    const popup = new mapboxgl.Popup({ closeButton: false })

    /** Static popup with closing button when browsing docs. */
    const place_popup = new mapboxgl.Popup({ className: 'place-popup' })

    /** keeps track of the application state: `['idle','loading','places','docs']`. */
    let state = 'idle'

    map.on('load', async () => {
        state = 'loading'
        const places = await query_sparql_places(map.getBounds(), 0, 100)

        map.addSource('gallica', {
            type: 'geojson',
            data: places,
            cluster: true,
            clusterMaxZoom: 14, // Max zoom to cluster points on
            clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
        })

        map.addLayer({
            id: 'clusters',
            type: 'circle',
            source: 'gallica',
            filter: ['has', 'point_count'],
            paint: {
                // Use step expressions (https://www.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
                'circle-color': [
                    'step',
                    ['get', 'point_count'],
                    '#0e397a', 3,
                    '#ea0b0b', 6,
                    '#ff8800', 10,
                    '#efec37'
                ],
                'circle-radius': [
                    'step',
                    ['get', 'point_count'],
                    15, 3,
                    30, 6,
                    50, 10,
                    80
                ]
            }
        })

        map.addLayer({
            id: 'unclustered-point',
            type: 'circle',
            source: 'gallica',
            filter: ['!', ['has', 'point_count']],
            paint: {
                'circle-color': '#fff',
                'circle-radius': 6,
                'circle-stroke-width': 1,
                'circle-stroke-color': '#0e397a'
            }
        })

        map.addLayer({
            id: 'cluster-count',
            type: 'symbol',
            source: 'gallica',
            filter: ['has', 'point_count'],
            paint: {
                'text-color': '#fff'
            },
            layout: {
                'text-field': '{point_count_abbreviated}',
                'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
                'text-size': 13
            },
        })

        state = 'places'

        /** requests a data refersh when map animations end. */
        map.on('moveend', async () => {
            if (state == 'loading') { return }

            state = 'loading'
            await refresh_data(map, 'gallica')
            state = 'places'
        })

        map.on('mouseenter', 'clusters', () => map.getCanvas().style.cursor = 'pointer')
        map.on('mouseleave', 'clusters', () => map.getCanvas().style.cursor = '')

        map.on('click', 'clusters', e => {
            const clusters = map.queryRenderedFeatures(e.point, { layers: ["clusters"] })
            const center = clusters[0].geometry.coordinates

            has_active_place = null
            map.easeTo({
                center: center,
                zoom: map.getZoom() + (16 - map.getZoom()) * .5
            })
        })

        /** updates popup properties when hovering a point.  */
        map.on('mouseenter', 'unclustered-point', e => {
            if (state == 'docs') { return }

            map.getCanvas().style.cursor = 'pointer'

            const feature = e.features[0]
            popup.setLngLat(feature.geometry.coordinates)
                .setText(feature.properties.label)
                .addTo(map)
        })

        map.on('mouseleave', 'unclustered-point', () => {
            if (state == 'docs') { return }

            map.getCanvas().style.cursor = ''
            popup.remove()
        })

        /** requests docs for a clicked place. */
        map.on('click', 'unclustered-point', async e => {
            const feature = e.features[0]
            if (has_active_place() === feature.properties.uri) { return }

            state = 'loading'
            popup.remove()
            place_popup.remove()

            map.easeTo({
                center: feature.geometry.coordinates,
                zoom: 16
            })

            place_popup.setLngLat(feature.geometry.coordinates)
                .setText(feature.properties.label)
                .addTo(map)

            place_popup.on('close', () => {
                document.querySelector('#docs').classList.remove('open')
                state = 'place'
            })

            const props = [
                ['and', 'bib.doctype', 'any', 'e'],
                ['or', 'bib.doctype', 'any', 'h'],
                ['or', 'bib.doctype', 'any', 'i'],
                ['or', 'bib.doctype', 'any', 'r']
            ]
            // (${docs.getElementsByTagName('srw:numberOfRecords')[0].innerHTML} total)

            const docs = await query_sru(feature.properties.label, props)
            const results = docs.getElementsByTagName('srw:record')

            state = 'docs'
            if (results.length <= 0) { return }
            update_docs(feature.properties, results)
        })

    })
})
